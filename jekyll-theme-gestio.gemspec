# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-gestio"
  spec.version       = "0.1.5"
  spec.authors       = ["Agustí B.R."]
  spec.email         = ["hola@agusti.cat"]

  spec.summary       = "Simple theme for pressus, factures i tickets"
  spec.homepage      = "http://agusti.cat"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.0"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.0"
  spec.add_development_dependency "jquery-rails", "~> 4.3.3"
  spec.add_development_dependency "bootstrap", "~> 4.1.3"
end
